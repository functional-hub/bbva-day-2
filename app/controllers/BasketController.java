package controllers;

import play.mvc.Result;

import static play.mvc.Results.notAcceptable;

public class BasketController {

    public Result calculateBasketTotal() {
        return notAcceptable();
    }

}
