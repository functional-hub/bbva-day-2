package controllers;

import play.mvc.Result;

import static play.mvc.Results.notAcceptable;

public class ClosureController {

    public Result repeatBob() {
        return notAcceptable();
    }

}
