package controllers;

import play.mvc.Result;

import static play.mvc.Results.notAcceptable;

public class CurryController {

    public Result curry() {
        return notAcceptable();
    }

}
