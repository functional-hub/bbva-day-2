package controllers;

import play.mvc.Result;

import static play.mvc.Results.notAcceptable;

public class FilterController {

    public Result doFilter() {
        return notAcceptable();
    }

}
