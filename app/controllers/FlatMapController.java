package controllers;

import play.mvc.Result;

import static play.mvc.Results.notAcceptable;

public class FlatMapController {

    public Result doFlatMap() {
        return notAcceptable();
    }

}
