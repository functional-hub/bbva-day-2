package controllers;

import play.mvc.Result;

import static play.mvc.Results.notAcceptable;

public class FoldController {

    public Result doFold() {
        return notAcceptable();
    }

}
