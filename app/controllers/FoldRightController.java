package controllers;

import play.mvc.Result;

import static play.mvc.Results.notAcceptable;

public class FoldRightController {

    public Result doFoldRight() {
        return notAcceptable();
    }

}
