package controllers;

import play.mvc.Result;

import static play.mvc.Results.notAcceptable;

public class HofController {

    public Result duplicate() {
        return notAcceptable();
    }

    public Result hello() {
        return notAcceptable();
    }

    public Result measure(String text) {
        return notAcceptable();
    }

}
