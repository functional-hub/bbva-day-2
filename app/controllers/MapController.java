package controllers;

import play.mvc.Result;

import static play.mvc.Results.notAcceptable;

public class MapController {

    public Result doMap() {
        return notAcceptable();
    }

}
