package models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Basket {

    private final Item[] items;

//    public Basket() {
//
//    }

    @JsonCreator
    public Basket(@JsonProperty("items") Item... items) {
        this.items = items;
    }

    public List<Item> getItems() {
        return Arrays.asList(items);
    }

    public static class Item {
        private final String name;
        private final int amount;

        @JsonCreator
        public Item(
            @JsonProperty("name") String name,
            @JsonProperty("amount") int amount
        ) {
            this.name = name;
            this.amount = amount;
        }

        public String getName() {
            return name;
        }

        public int getAmount() {
            return amount;
        }
    }

}
