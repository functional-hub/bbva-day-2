package models;

public class BasketTotal {

    private final int total;

    public BasketTotal(int total) {
        this.total = total;
    }

    public int getTotal() {
        return total;
    }
}
