package models;

import play.libs.Json;

public class DemoValues {

    public static final String basket = Json.prettyPrint(
        Json.toJson(
            new Basket(
                new Basket.Item("Bread", 37),
                new Basket.Item("Milk", 65),
                new Basket.Item("Cookies", 200)
            )
        )
    );

}
