package models;

import static java.util.Arrays.copyOfRange;

public abstract class List<T> {

    public static <T> List<T> emptyList() {
        return new Empty<>();
    }

    public static <T> List<T> listOf(T item) {
        return listOf(item, emptyList());
    }

    public static <T> List<T> listOf(T item, List<T> tail) {
        return new NonEmpty<>(item, tail);
    }

    public static <T> List<T> listOf(T... items) {
        switch (items.length) {
            case 0:
                return emptyList();
            case 1:
                return listOf(items[0]);
            default:
                T[] tail = copyOfRange(items, 1, items.length);
                T head = items[0];
                return listOf(head, listOf(tail));
        }

    }

    public static final class Empty<T> extends List<T> {
    }

    public static final class NonEmpty<T> extends List<T> {

        private final T head;
        private final List<T> tail;

        public NonEmpty(T head, List<T> tail) {
            this.head = head;
            this.tail = tail;
        }

        public T getHead() {
            return head;
        }

        public List<T> getTail() {
            return tail;
        }
    }

}
