package models;

public class Session {

    private final boolean anonymous;
    private final boolean premium;
    private final boolean banned;

    public Session(boolean anonymous, boolean premium, boolean banned) {
        this.anonymous = anonymous;
        this.premium = premium;
        this.banned = banned;
    }

    public boolean isAnonymous() {
        return anonymous;
    }

    public boolean isPremium() {
        return premium;
    }

    public boolean isBanned() {
        return banned;
    }

    public Session toggleAnonymous() {
        return new Session(!anonymous, premium, banned);
    }

    public Session togglePremium() {
        return new Session(anonymous, !premium, banned);
    }

    public Session toggleBanned() {
        return new Session(anonymous, premium, !banned);
    }

}

