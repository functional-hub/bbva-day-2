package service;

import models.User;

import java.util.Arrays;
import java.util.List;

import static models.User.State.ACTIVE;
import static models.User.State.INACTIVE;

public class UserService {

    private User[] hardcodedUsers = {
        new User(1, "Bruce", INACTIVE),
        new User(2, "Rachel", INACTIVE),
        new User(3, "Brian", ACTIVE),
        new User(4, "Susan", INACTIVE),
        new User(5, "Louise", ACTIVE),
        new User(6, "Deborah", INACTIVE),
        new User(7, "Jennifer", INACTIVE),
        new User(8, "Joyce", INACTIVE),
        new User(9, "Julie", INACTIVE),
        new User(10, "Lillian", ACTIVE),
        new User(11, "Wanda", INACTIVE),
        new User(12, "Phyllis", INACTIVE),
        new User(13, "Justin", INACTIVE),
        new User(14, "Christopher", ACTIVE),
        new User(15, "Martha", ACTIVE),
        new User(16, "Rachel", INACTIVE),
        new User(17, "Shirley", ACTIVE),
        new User(18, "Phyllis", INACTIVE),
        new User(19, "Todd", INACTIVE),
        new User(20, "Eric", ACTIVE),
        new User(21, "Jonathan", INACTIVE),
        new User(22, "Frank", ACTIVE),
        new User(23, "Ernest", INACTIVE),
        new User(24, "Christine", INACTIVE),
        new User(25, "Helen", ACTIVE),
        new User(26, "Albert", INACTIVE),
        new User(27, "Carol", INACTIVE),
        new User(28, "Harold", INACTIVE),
        new User(29, "Arthur", INACTIVE),
        new User(30, "Stephen", INACTIVE)
    };

    public List<User> getAllUsers() {
        return Arrays.asList(hardcodedUsers);
    }

}
