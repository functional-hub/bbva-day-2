package views;

public class HtmlView {

    public static String link(String href, String text) {
        return "<a href=\"" + href + "\">" + text + "</a>";
    }

    public static String p(String text) {
        return "<p>" + text + "</p>";
    }

    public static String newLine() {
        return "</br>";
    }

}
